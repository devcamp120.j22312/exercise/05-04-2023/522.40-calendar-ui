import KeyboardDoubleArrowLeftIcon from '@mui/icons-material/KeyboardDoubleArrowLeft';
import KeyboardDoubleArrowRightIcon from '@mui/icons-material/KeyboardDoubleArrowRight';
import KeyboardArrowLeftIcon from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import {StyledButton} from './style'
const Calendar = () =>{
    return (
        <div className='main'>
          <div className='card'>
            <div className='header'>
              <StyledButton type='normal'><KeyboardDoubleArrowLeftIcon style={{fontSize: "15px"}}/></StyledButton>
              <StyledButton type='normal'><KeyboardArrowLeftIcon style={{fontSize: "15px"}}/></StyledButton>
              <StyledButton type='normal' style={{width: "150px"}}>February 2018</StyledButton>
              <StyledButton type='normal'><KeyboardDoubleArrowRightIcon style={{fontSize: "15px"}}/></StyledButton>
              <StyledButton type='normal'><KeyboardArrowRightIcon style={{fontSize: "15px"}}/></StyledButton>
            </div>
            <div className='content'>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>SUN</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>MON</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>TUE</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>WED</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>THU</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>FRI</StyledButton>
              <StyledButton type='normal' style={{fontWeight: "bold"}}>SAT</StyledButton>
            </div>
            <div className='footer'>
              <StyledButton>28</StyledButton>
              <StyledButton>29</StyledButton>
              <StyledButton>30</StyledButton>
              <StyledButton>31</StyledButton>
              <StyledButton type='normal'>1</StyledButton>
              <StyledButton type='normal'>2</StyledButton>
              <StyledButton type='normal' style={{color: "white", backgroundColor: "rgb(0,110,221)"}}>3</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>4</StyledButton>
              <StyledButton type='normal'>5</StyledButton>
              <StyledButton type='normal'>6</StyledButton>
              <StyledButton type='normal'>7</StyledButton>
              <StyledButton type='normal'>8</StyledButton>
              <StyledButton type='normal'>9</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>10</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>11</StyledButton>
              <StyledButton type='normal'>12</StyledButton>
              <StyledButton type='normal'>13</StyledButton>
              <StyledButton type='normal'>14</StyledButton>
              <StyledButton type='normal'>15</StyledButton>
              <StyledButton type='normal'>16</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>17</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>18</StyledButton>
              <StyledButton type='normal'>19</StyledButton>
              <StyledButton type='normal'>20</StyledButton>
              <StyledButton type='normal'>21</StyledButton>
              <StyledButton type='normal'>22</StyledButton>
              <StyledButton type='normal'>23</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>24</StyledButton>
              <StyledButton type='normal' style={{color: "red"}}>25</StyledButton>
              <StyledButton type='normal'>26</StyledButton>
              <StyledButton type='normal'>27</StyledButton>
              <StyledButton type='normal'>28</StyledButton>
              <StyledButton>1</StyledButton>
              <StyledButton>2</StyledButton>
              <StyledButton>3</StyledButton>
            </div>
          </div>
        </div>
      );
}
export default Calendar;
